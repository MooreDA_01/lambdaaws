
import boto3

rekognition = boto3.client('rekognition')

def lambda_handler(event, context):
    #print (event)

    bucket = event['Records'][0]['s3']['bucket']['name']
    photo = event['Records'][0]['s3']['object']['key']

    response = rekognition.detect_faces(Image={'S3Object':{'Bucket':bucket, 'Name':photo}}, Attributes=['All'])
    
    print('There are '+ str(len(response['FaceDetails']))+"people in this photo.")
    for faceDetail in response['FaceDetails']:
        if faceDetail['Smile']['value'] == true:
            print("This person is smilling....")
        else:
            print("This person is not smilling....")